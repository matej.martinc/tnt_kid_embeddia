# Code for experiments conducted in the paper 'TNT-KID: Transformer-based Neural Tagger for Keyword Identification' submitted to Natural Language Engineering journal #

Please cite the following paper [[bib](https://gitlab.com/matej.martinc/tnt_kid/-/blob/master/bibtex.js)] if you use this code:

Matej Martinc, Blaz Skrlj and Senja Pollak. TNT-KID: Transformer-based Neural Tagger for Keyword Identification. arXiv preprint arXiv:2003.09166.


## Installation, documentation ##

Instructions for installation assume the usage of PyPI package manager.<br/>
To get the source code and the example train and test data, clone the project from the repository with 'git clone https://gitlab.com/matej.martinc/tnt_kid_embeddia' <br/>


Install dependencies if needed: pip install -r requirements.txt

### To extract keywords with the model trained on English news (KPTimes dataset): ###

```
python predict.py --data_path data/example.json --bpe_model_path bpe/bpe_english.model --dict_path dictionaries/dict_lm+bpe+rnn_english.ptb --trained_classification_model trained_classification_models/model_lm+bpe+rnn_english.pt --lang english --adaptive --rnn --bpe --cuda
```
You also have classification models, dictionaries and byte-pair tokenization models available for Croatian, Estonian and Russian (see folders 'trained_classification_models', 'dictionaries' and 'bpe') that you can use. Just change --lang parameter to 'croatian', 'estonian' or 'russian' accordingly when you use these models. 

There is also a Jupyter notebook predict.ipynb, where you can run the predict script step by step and get to know the script a bit better.

### You can also train your own keyword detection model: ###

Generate Sentencepiece byte-pair encoding model:<br/>
```
python bpe.py --input data/kptimes/kptimes_lm.json --output bpe/bpe_kptimes
```

Train language model on the KPTimes English news articles:<br/>
```
python train_and_eval.py --config_id kptimes_news --lm_data_path data/kptimes/kptimes_lm.json --bpe_model_path bpe/bpe_kptimes.model --adaptive --rnn --bpe --cuda
```

Train and test the keyword tagger on the KPTimes datasets from the news domain:<br/>
```
python train_and_eval.py --config_id kptimes_news --train_data_path data/kptimes/kptimes_train.json --test_data_path data/kptimes/kptimes_test.json --bpe_model_path bpe/bpe_kptimes.model --classification --transfer_learning --rnn --bpe --cuda
```

Note that config_id argument is used to connect a specific language model with the classification model. It can be whatever you like but needs to be the same for language model training and keyword tagger training so that the keyword tagger knows which language model to use. <br/><br/>

You can also train models for other languages. Besides English, the code supports Croatian, Estonian and Russian. Just use the argument --lang 
## Contributors to the code ##

Matej Martinc<br/>

* [Knowledge Technologies Department](http://kt.ijs.si), Jožef Stefan Institute, Ljubljana
